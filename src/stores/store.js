import { AsyncStorage } from 'react-native';
import devTools from 'remote-redux-devtools';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { persistStore } from 'redux-persist'

import reducers from '../reducers/combineReducers';

const App = require('../../package.json');

const middlewares = [thunk];

// Add redux-logger to middlewares only in Development Environment
if (process.env.NODE_ENV === 'development') {
  const { logger } = require('redux-logger');

  middlewares.push(logger);
}

export default createStore(
  // Takes reducer as first param
  reducers,
  // And then takes list of arguments. These are called middlewares and 
  // they can do various things. Basicly these middlewares will be placed to
  // redux update cycle and they will get evry action that gets triggered. They 
  // can also affect to outcome.

  // Link our app to redux-dev-tools
  devTools({
    name: App.name,
    hostname: 'localhost',
    port: 8081,
    realtime: true,
  }),
  // Apply thunk to our redux
  applyMiddleware(...middlewares)
)