// @flow

import axios from 'axios';
import { apiPath } from '../configs/configs'
// Add a response interceptor
// Make sure to comment out in production
axios.interceptors.response.use(function (response) {
    return response;
   }, function (error) {
    return Promise.reject(error);
   });

// Helper function for get request   
const getRequest = (path: string): Promise<*> =>
 axios
   .get(`${apiPath}/${path}`)
   .then((response: Object): Object => response)
   .catch((error: Object): Promise<Object> => {
     return Promise.reject({error: error});
   });
// Helper function for post request   
const postRequest = (path: string, data: Object): Promise<*> =>
 axios
   .post(`${apiPath}/${path}`, data)
  //  .then((response: Object): Object => response)
   .catch((error: Object): Object => {
     return Promise.reject({error: error});
   });

// Get Home Data from server
export const getHomeData = (): Promise<Object> =>
  getRequest('5acb255e2f00005b004113eb')
     .then((response: Object): Object => response)
     .catch((error: Object): Promise<Object> => error)